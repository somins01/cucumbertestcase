package com.somins.app;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.activation.MailcapCommandMap;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Scope(value="prototype")
	@RequestMapping(value = "/rest/getDatas", method = RequestMethod.GET ,headers="Accept=application/json",produces={"application/json"}) 
	@ResponseBody   @Order(1) public String getDatas() throws JsonGenerationException, JsonMappingException, IOException {
		 ObjectMapper obj=new ObjectMapper();
		 Map<String, String> aon=new HashMap<String, String>();
		 aon.put("somins","shukla");
		 String objaa=obj.writeValueAsString(aon);
		/* JSONObject request = new JSONObject();
		 request.put("username", name);
		 request.put("password", password);*/
		 
		 JSONObject request = new JSONObject();
		 
		 JSONObject user = new JSONObject();
	
		
		 request.put("name", "aaa");
		 request.put("roll", "testing");
		 user.put("user", request);
		
		 RestTemplate restTemplate = new RestTemplate();
		  
		 HttpHeaders headers = new HttpHeaders();
		 headers.setContentType(MediaType.APPLICATION_JSON);
		   System.out.println("user"+user.toString());
		    HttpEntity<String> entity = new HttpEntity<String>(user.toString(), headers);
		ResponseEntity<String> resp=   restTemplate.postForEntity("http://localhost:8080/app/rest/create", entity, String.class) ;
		System.out.println("response"+resp.getBody());
		return objaa;
	}
	
	
	
	@RequestMapping(value = "/rest/pathvariabe", method = RequestMethod.GET ,headers="Accept=application/json",produces={"application/json"})
	@ResponseBody public String requestParam(@RequestParam("name") String name)throws JsonGenerationException, JsonMappingException, IOException {
		 ObjectMapper obj=new ObjectMapper();
		 Map<String, String> aon=new HashMap<String, String>();
		 aon.put("somins",name);
		 String objaa=obj.writeValueAsString(aon);
		
		return objaa;
	}
	@RequestMapping(value="/rest/create", method=RequestMethod.POST)
	@ResponseBody public String createUser(@RequestBody SominsDto user) throws JsonGenerationException, JsonMappingException, IOException{
		 ObjectMapper obj=new ObjectMapper();
		 Map<String, Object> aon=new HashMap<String, Object>();
		 aon.put("somins",user);
		 String objaa=obj.writeValueAsString(aon);
		
		return objaa;
	}
	
	
	@RequestMapping(value = "/rest/postPath", method = RequestMethod.POST ,headers="Accept=application/json",produces={"application/json"})
	@ResponseBody public String postoperation(@RequestParam("name") String name)throws JsonGenerationException, JsonMappingException, IOException {
		 ObjectMapper obj=new ObjectMapper();
		System.out.println("name wil be"+name);
		 Map<String, String> aon=new HashMap<String, String>();
		 aon.put("somins",name);
		 String objaa=obj.writeValueAsString(aon);
		
		return objaa;
	}
	
}
