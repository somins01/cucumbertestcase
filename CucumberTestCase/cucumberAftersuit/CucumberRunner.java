package restassuredexample.cucumber;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import cucumber.api.junit.Cucumber;
import cucumber.runtime.Glue;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;


@RunWith(CustomCucumberRunner.class)
@CucumberOptions(
plugin = { 
	    "html:target/cucumber-html-report",
	    "json:target/cucumber.json"
	        },
//(format={"json:target/cucumber.json"}
    features = {"src/test/resources"},strict = true,
    dryRun= false,monochrome = true, snippets= SnippetType.CAMELCASE
    	
		
		
)
public class CucumberRunner {
	 @AfterSuite
     public static void generateReport(){
        File reportOutputDirectory = new File("target");
        List<String> jsonFiles = new ArrayList<>();
        //jsonFiles.add("target/cucumber-usage.json");
        jsonFiles.add("target/cucumber.json");
      //  jsonFiles.add("target/cucumber-usage.json");

        String jenkinsBasePath = "CucumberTestCase";
        String buildNumber = "1";
        String projectName = "";
        boolean skippedFails = true;
        boolean pendingFails = false;
        boolean undefinedFails = true;
        boolean missingFails = true;
        boolean runWithJenkins = false;
        boolean parallelTesting = false;
System.out.println("hello i am coming");
        Configuration configuration = new Configuration(reportOutputDirectory, projectName);
     // optional configuration
        configuration.setParallelTesting(parallelTesting);
        configuration.setRunWithJenkins(runWithJenkins);
        configuration.setBuildNumber(buildNumber);
        // addidtional metadata presented on main page
        configuration.addClassifications("Platform", "Windows");
        configuration.addClassifications("Browser", "Firefox");
        configuration.addClassifications("Branch", "release/1.0");

       ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
     //  ReportBuilder    reportBuilder=new 
        reportBuilder.generateReports();
    }

}