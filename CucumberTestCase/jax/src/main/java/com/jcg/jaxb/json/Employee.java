/**
 * 
 */
package com.jcg.jaxb.json;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author ashraf_sarhan
 * 
 */
@XmlRootElement(name="EMPLOYEE")
public class Employee {
	@XmlAttribute(name = "ID",required=true) 
	private int id;
	@XmlAttribute(name = "NAME",required=true) 
	private String name;

	private List<String> skills;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getSkills() {
		return skills;
	}

	public void setSkills(List<String> skills) {
		this.skills = skills;
	}

}
