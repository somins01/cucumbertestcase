package restassuredexample.cucumber;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class InternationalSpaceStationCurrentLocationDefinition {

	public InternationalSpaceStationCurrentLocationDefinition() {
		RestAssured.baseURI = InternationalSpaceStationCurrentLocationConfiguration.OPEN_NOTIFY_API_URI;

		RestAssured.authentication = preemptive().basic("live_897e0bd18733e2730f60117b8236cb40b3824342", "");
	}

	public void requestInternationalSpaceStationCurrentLocation() {
	
		Response response = given()
				.contentType("application/json").

				when().get("/api/v1").then().statusCode(200).extract().response();
	}

	public void validateInternationalSpaceStationCurrentLocationContents() {
		Response response = given()
				.contentType("application/json").when().get("/api/v1").then().body(containsString("documentation"))
				.body(containsString("api_version")).

				body(("api_version"), equalTo("1.0")).extract().response();
	}
}