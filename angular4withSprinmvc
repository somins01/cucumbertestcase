	
Single Page Apps with Atom
by  Collis, AdisaNo presence information  on 6/26/2017 4:41 PM
Angular is a rich and feature-filled JavaScript framework. This post will help you to get started with building Angular applications.​ You will need to have Node.js and NPM installed. Visit the Angular website - angular.io for features, documentation, and additional resources.

Use the official Angular style guide for preferred conventions while building your app: https://angular.io/guide/styleguide



If you’ve followed the Atom 4.X Developer Setup Guide You’ll be able to use the Atom Eclipse Project Generation Plugin from within Eclipse/STS
File > New > Other
Type: Atom and select “New Atom Web Application”​
  

Use NPM to Install the Angular CLI from the command line
npm install -g @angular/cli
angular cli documentation: https://github.com/angular/angular-cli/wikis
Create a directory src\main\app in your Atom project
From the command line, go to the src\main\app directory and use the Angular CLI to create your Angular project
ng new my-app-name --routing --skip-install –skip-git
You want to use --skip-install since installation will occur later in the process
A detailed list of all Angular CLI options can be found here https://github.com/angular/angular-cli/wiki/new
If using the node.js server while testing, set up a proxy for making GET calls to your API
Create a json file at the root of the angular project and name it proxy.conf.json
\src\main\app\my-app-name\proxy.conf.json
When starting the node server add the --proxy-config flag with the path to the proxy.conf.json file
--proxy-config ./src/main/app/my-app-name/proxy.conf.json
more info for setting up the proxy: https://github.com/angular/angular-cli/wiki/stories-proxy
example proxy.conf.json file:
{
   "/api": {
        "target": "https://tiaa-myhostname.ad.tiaa-cref.org/private/employeesample",
        "secure": false
    }
}

 

Move the generated package.json file from \src\main\app\my-app-name\package.json to the root of your Atom project (To the same level as build.gradle)
Open the package.json file and adjust the settings in the 'scripts' section
This is an example, adjust it to fit your app:
 

"scripts": {
    "ng": "ng",
    "start": "ng serve --host tiaa-myhostname.ad.tiaa-cref.org --proxy-config ./src/main/app/my-app-name/proxy.conf.json --ssl --watch --sourcemap --open",
    "build": "webpack",
    "test": "npm run lint && ng test",
    "lint": "tslint 'src/main/app/my-app-name/app/**/*.ts' --force",
    "lint:fix": "tslint 'src/main/app/my-app-name /app/**/*.ts' --fix --force",
    "e2e": "protractor src/main/app/my-app-name/protractor.conf.js"
  }

 

Move the generated .angular-cli.json file from \src\main\app\my-app-name\.angular-cli.json to the root of your Atom project (To the same level as build.gradle)
In the "apps" section, change the paths for "root" and "outDir" to the correct paths for your project
 

"root": "./src/main/app/my-app-name/src",
"outDir": "./target/generated-sources/main/webapp/app/my-app-name",

 

Change the paths to the config files for end-to-end testing, linting, and unit testing
 

"e2e": {
    "protractor": {
      "config": "./src/main/app/my-app-name/protractor.conf.js"
    }
  },
  "lint": [
    {
      "project": "./src/main/app/my-app-name/src/tsconfig.app.json"
    },
    {
      "project": "./src/main/app/my-app-name/src/tsconfig.spec.json"
    },
    {
      "project": "./src/main/app/my-app-name/e2e/tsconfig.e2e.json"
    }
  ],
  "test": {
    "karma": {
      "config": "./src/main/app/my-app-name/karma.conf.js"
    }
  }

 

Add an index.ftl template file \src\main\app\my-app-name\src\index.ftl
 

<#macro main>
    <app-root></app-root>
</#macro>
<#macro pagebody>
    <% for (key in htmlWebpackPlugin.files.chunks) { %>
        <script src="<@spring.url '<%= htmlWebpackPlugin.files.chunks[key].entry %>'/>" type="text/javascript"></script>
    <% } %>
</#macro>
<@atom.render pageTitle="SPA Example" layoutType="responsive" />

  

Add the webpack.config.js that is attached​ to this post to the root of the Atom project
Change the paths within the webpack file to match your project
Add a file named .npmrc to the root of the Atom project to set the proxy for npm
Add the following to the .npmrc file (These values will change when Artifactory gets upgraded later this year)
registry=https://registry.npmjs.org
proxy=http://proxy.ops.tiaa-cref.org:8080/
https-proxy=http://proxy.ops.tiaa-cref.org:8080/

 

In the build.gradle file add: apply plugin: 'atom-spa'
Import the required node modules by building the project: gradle tasks menu > spaGeneration

To serve the Angular app on a test Node.js server run this command from the command line at the root of your Atom project: npm run start

To build the project run the gradle spaGeneration task: gradle tasks menu > spaGeneration​
webpack.config.jswebpack.config.js

  

 Permanent Link to Post | Email Post Link | Number of Comments 0 Comment(s)
